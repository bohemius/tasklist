package models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by bohemius on 28. 9. 2015.
 */

@Entity
@Table(name = "TASK")
public class Task {

    public Task() {
        this.setStatus(false);
        this.setPriority(Priority.Normal);
    }

    public Task(String text) {
        this();
        this.text = text;
    }

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;

    @Enumerated(EnumType.STRING)
    private Priority priority;

    private boolean status;
    private String text;

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Priority getPriority() {

        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
