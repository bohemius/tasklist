package controllers;

import play.mvc.Controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.text.html.parser.Entity;

/**
 * Created by bohemius on 28. 9. 2015.
 */
public class ModelController extends Controller {
    private EntityManagerFactory emf=null;
    private EntityManager em=null;

    public EntityManager getEm() {
        if (this.em == null)
            this.em = getEmf().createEntityManager();
        return this.em;
    }

    public EntityManagerFactory getEmf() {
        if (this.emf == null)
            this.emf = Persistence.createEntityManagerFactory("tasklist");
        return this.emf;
    }
}
