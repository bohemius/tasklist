package controllers;

import jdk.nashorn.internal.ir.annotations.Immutable;
import models.Task;
import play.mvc.*;

import views.html.*;

import java.util.List;

public class Application extends ModelController {

    public Result index() {
        Result r = internalServerError("Sorry, internal application error.");

        try {
            List<Task> taskList=null;

            getEm().clear();
            getEm().getTransaction().begin();
            taskList=getEm().createQuery("from Task ", Task.class).getResultList();
            getEm().getTransaction().commit();

            if (taskList != null)
                r=ok(tasklist.render(taskList));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return r;
        }
    }

}
