package controllers;

import com.wordnik.swagger.annotations.*;
import models.Priority;
import models.Task;
import play.libs.Json;
import play.mvc.Result;

/**
 * Created by bohemius on 28. 9. 2015.
 */
@Api(value = "/task", description = "Task list management functions")
public class TaskListController extends ModelController {

    @ApiOperation(nickname = "status",
            value = "Modifies task from the task list",
            notes = "Changes the status of the task.",
            httpMethod = "PUT",
            consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", access = "Task ID", dataType = "long", paramType = "path", required = true),
            @ApiImplicitParam(name = "status", access = "Resolution state (true/false)", dataType = "boolean", paramType = "header", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 500, message = "There was a problem modifying task"),
            @ApiResponse(code = 200, message = "Task modified"),
            @ApiResponse(code = 404, message = "Task not found"),
    })
    public Result changeStatus(long taskId) {
        Result r = internalServerError("There was a problem modifying task");

        try {
            Task task = fetchTask(taskId);
            boolean status = Boolean.parseBoolean(request().getHeader("status"));

            if (task != null) {
                getEm().getTransaction().begin();
                task.setStatus(status);
                getEm().persist(task);
                getEm().getTransaction().commit();
            } else
                r = notFound("Task " + taskId + " does not exist");

            r = ok("Task " + taskId + " status changed to " + status);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return r;
        }
    }

    @ApiOperation(nickname = "priority",
            value = "Modifies task from the task list",
            notes = "Changes the priority (Low,Normal,Urgent,Critical) of the task.",
            httpMethod = "PUT",
            consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", access = "Task ID", dataType = "long", paramType = "path", required = true),
            @ApiImplicitParam(name = "priority", access = "Priority (Low,Normal,Urgent,Critical)", dataType = "string", paramType = "header", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 500, message = "There was a problem modifying task"),
            @ApiResponse(code = 200, message = "Task modified"),
            @ApiResponse(code = 404, message = "Task not found"),
            @ApiResponse(code = 400, message = "Bad request")
    })
    public Result changePriority(long taskId) {
        Result r = internalServerError("There was a problem removing task");

        try {
            Task task = fetchTask(taskId);
            String priority = request().getHeader("priority");

            if (task != null) {
                task.setPriority(Priority.valueOf(priority));
                getEm().getTransaction().begin();
                getEm().persist(task);
                getEm().getTransaction().commit();
            } else
                r = notFound("Task " + taskId + " does not exist");

            r = ok("Task " + taskId + " priority changed to " + priority);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            r = badRequest("Unknown priority, supported priorities are Low, Normal, Urgent, Critical");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return r;
        }
    }

    @ApiOperation(
            value = "Adds a new task to the task list",
            notes = "Task text is supplied in http header and is required. Returns the ID of created task",
            httpMethod = "POST",
            consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskText", access = "Task description", dataType = "string", paramType = "header", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 500, message = "There was a problem adding task"),
            @ApiResponse(code = 201, message = "Task ID returned"),
    })
    public Result addTask() {
        Result r = internalServerError("There was a problem adding task");

        try {
            String text = request().getHeader("taskText");
            Task task = new Task(text);

            getEm().getTransaction().begin();
            getEm().persist(task);
            getEm().getTransaction().commit();

            r = created(Long.toString(task.getId()));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return r;
        }
    }

    @ApiOperation(
            value = "Removes task from the task list",
            notes = "Task must exist otherwise 404 is returned.",
            httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", access = "Task ID", dataType = "long", paramType = "path", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 500, message = "There was a problem removing task"),
            @ApiResponse(code = 200, message = "Task removed"),
            @ApiResponse(code = 404, message = "Task not found"),
    })
    public Result removeTask(long taskId) {
        Result r = internalServerError("There was a problem removing task");

        try {
            Task task = fetchTask(taskId);

            if (task != null) {
                getEm().getTransaction().begin();
                getEm().remove(task);
                getEm().getTransaction().commit();
            } else
                r = notFound("Task " + taskId + " does not exist");
            r = ok("Task " + taskId + " removed from task list ");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return r;
        }
    }

    @ApiOperation(
            value = "Retrieves task from the task list",
            notes = "Task must exist otherwise 404 is returned.",
            httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", access = "Task ID", dataType = "long", paramType = "path", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 500, message = "There was a problem getting task"),
            @ApiResponse(code = 200, message = "Task retrieved", response = Task.class),
            @ApiResponse(code = 404, message = "Task not found"),
    })
    public Result getTask(long taskId) {
        Result r = internalServerError("There was a problem getting task");

        try {
            Task task = fetchTask(taskId);

            if (task != null)
                r = ok(Json.toJson(task));
            else
                r = notFound("Task " + taskId + " does not exist");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return r;
        }
    }

    private Task fetchTask(long id) {
        return getEm().find(Task.class, id);
    }
}
