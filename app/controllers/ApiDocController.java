package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;


/**
 * Created by bohemius on 30. 8. 2015.
 */
public class ApiDocController extends Controller {
    public Result swagger() {
        return ok(swagger.render());
    }
}
