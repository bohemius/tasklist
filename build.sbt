name := """TaskList"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "pl.matisoft" %% "swagger-play24" % "1.4",
  "org.hibernate" % "hibernate-entitymanager" % "5.0.1.Final",
  "com.h2database" % "h2" % "1.4.189"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
